define([
	'components/components'
], function(
	componentManager
) {
	return {
		name: 'Component Loader',

		extraScripts: [
			// none
		],

		init: function () {
			// nothing to set up
		},
		
		load: function (cpn) {
			// have to use .init(callback) since it crashes when trying to call the onReady callback which won't exist unless done through .init(callback)
			componentManager.init(() => {
				// prevents crash with not being able to call .spliceWhere on the waiting array (which is deleted after all mods are loaded)
				//     the reason why it is deleted is since we are loading more components after the core server loads the core components
				//     which means we have to create it if it has been deleted
				componentManager.waiting = componentManager.waiting || [];
				
				// if the component hasn't already been loaded...
				if(!componentManager.components[cpn.type]) {
					// ...then load it
					componentManager.onGetComponent(cpn);
				}
			});
		}
	};
});
